# TSRProject

1. Discover TwinSocial Rest API: https://twinesocial.com/developers/rest
2. Setup venv with requirements:
* webpack
* bootstrap
* tornado
* react
* nodeJS
3. Create .jsx templates with reactJS and nodeJS
4. Build created .jsx templates using webpack
5. Make url rooting for your app using tornado
6. Setup docker
7. Create image of your app using docker
8. Deploy docker image to any free hosting